import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Router from "./components/Router";

// Render pages based on configurations in Router.js
ReactDOM.render(<Router />, document.getElementById('root'));

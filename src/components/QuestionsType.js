import React, { Component } from 'react';

class Author extends Component {

    state = {
        currentRadioSelection: 0
    }

    // Update the selection value in Author.js
    handleTypeSelection = (e) => {
        const value = +e.currentTarget.value;
        this.setState({
            currentRadioSelection: value
        });
        this.props.updateQuestionType(value);
    }

    render() {
        return (
            <div className="question-type-container MB4x">
                <h4>What type of question you want to create?</h4>
                <form>
                    <p>
                        <input id="mcq" type="radio" name="q_type" value="0" checked={this.state.currentRadioSelection === 0} onChange={this.handleTypeSelection} />
                        <label htmlFor="mcq">Multiple Choice Question</label>
                    </p>
                    <p>
                        <input id="stq" type="radio" name="q_type" value="1" checked={this.state.currentRadioSelection === 1} onChange={this.handleTypeSelection} />
                        <label htmlFor="stq">Submission Type Question</label>
                    </p>
                    <p>
                        <input id="ptq" type="radio" name="q_type" value="2" checked={this.state.currentRadioSelection === 2} onChange={this.handleTypeSelection} />
                        <label htmlFor="ptq">Passage Type Question</label>
                    </p>
                </form>
            </div>
        )
    }
}

export default Author;

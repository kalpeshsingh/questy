import React, { Component } from 'react';

class Login extends Component {

    state = {
        currentRadioSelection: 0
    }

    handleLoginAs = (e) => {
        const value = +e.currentTarget.value;
        this.setState({
            currentRadioSelection: value
        })
    }

    handleLogin = () => {
        this.props.updateLoginRole(this.state.currentRadioSelection);
    }

    render() {
        const loggedInAs = this.state.currentRadioSelection === 0 ? 'Student' : 'Teacher';
        return(
            <div className="container login-container">
                <div className="login-container__header">🔐 Please select login</div>
                <div className="login-container__body">
                    <form>
                        <p>
                            <input id="student_login" type="radio" name="loginAs" value="0" checked={this.state.currentRadioSelection === 0} onChange={this.handleLoginAs} />
                            <label htmlFor="student_login">Student <span aria-hidden="true">👨‍💻</span></label>
                        </p>
                        <p>
                            <input id="teacher_login" type="radio" name="loginAs" value="1" checked={this.state.currentRadioSelection === 1} onChange={this.handleLoginAs} />
                            <label htmlFor="teacher_login">Teacher <span aria-hidden="true">👨‍🏫</span></label>
                        </p>
                    </form>
                </div>
                <div className="login-container__footer">
                    <button onClick={this.handleLogin}>Login as {loggedInAs}</button>
                </div>
            </div>
        );
    }
}

export default Login;
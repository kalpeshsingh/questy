import React, { Component } from 'react';
import Login from './Login';

class App extends Component {

  state = {
    role: null
  }

  // Update login role, will be used in future component developments
  updateLoginRole = (role) => {
    this.setState({
      role
    });
    if(role === 0) {
      this.props.history.push(`student`);
    } else {
      this.props.history.push(`author`);
    }
  }

  render() {
    return(
      <Login updateLoginRole={this.updateLoginRole} />
    )
  }
}

export default App;

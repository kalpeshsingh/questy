import React, { Component } from 'react';

class Passage extends Component {
    render() {
        return (
            <React.Fragment>
                <label htmlFor="input_ideal_answer">Ideal Answer</label>
                <input id="input_ideal_answer" type="text" placeholder="Type your ideal answer..." />
            </React.Fragment>
        )
    }
}

export default Passage;
import React, { Component } from 'react';

class QuestionSet extends Component {
    // A generic code to power all three types of questions. 
    // I'm proud of this code. 😎
    render() {
        return (
            <form className="vertical-form">
                <div className="MB2x">
                    <label htmlFor="input_title">Question Title</label>
                    <input id="input_title" type="text" placeholder="Type your question title..." />
                </div>
                <div className="MB2x">
                    <label htmlFor="input_desc">Question Description</label>
                    <input id="input_desc" type="text" placeholder="Type your question description..." />
                </div>
                <div className="MB2x">
                    {this.props.children}
                </div>
                <div>
                    <label htmlFor="input_instructions">Instructions</label>
                    <input id="input_instructions" type="text" placeholder="Type Instructions (Example - Max. file 5 MB upload.)" />
                </div>
            </form>
        )
    }
}

export default QuestionSet;
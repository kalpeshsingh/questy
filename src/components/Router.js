import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import App from "./App";
import Author from "./Author";
import Student from "./Student";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App} />
      <Route exact path="/author" component={Author} />
      <Route exact path="/student" component={Student} />
    </Switch>
  </BrowserRouter>
);

export default Router;
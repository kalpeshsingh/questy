import React, { Component } from 'react';

class Mcq extends Component {

    options = [{
        label: "Option A",
        value: "A"
    }, {
        label: "Option B",
        value: "B"
    }, {
        label: "Option C",
        value: "C"
    }, {
        label: "Option D",
        value: "D"
    }]

    render() {
        return (
            <div className="mcq-container">
                <div className="mcq-container__title">
                    <div className="mcq-container__left">Answer Options</div>
                    <div className="mcq-container__right">Right Answers</div>
                </div>
                <div>
                    {
                        this.options.map((item, idx) => {
                            return(
                                <div key={idx} className="mcq-container__item">
                                <div className="mcq-container__left">
                                    <label className="visually-hidden">{item.label}</label>
                                    <input type="text" placeholder={`Type Option ${item.value}...`} />
                                </div>
                                <div className="mcq-container__right">
                                    <input type="checkbox" name="" value={item.value} />
                                    <label className="visually-hidden">{item.label}</label>
                                </div>
                            </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Mcq;
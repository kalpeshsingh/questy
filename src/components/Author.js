import React, { Component } from 'react';
import QuestionsType from './QuestionsType';
import QuestionSet from './QuestionSet';
import Mcq from './Mcq';
import Passage from './Passage';

class Author extends Component {

  state = {
    currentQuestionUI: 0
  }

  // Render UI based on question type selection
  renderQuestionUI = () => {
    const currentQuestionUI = this.state.currentQuestionUI;
    switch (currentQuestionUI) {
      case 0:
        return (
          <Mcq />
        )
      case 1:
        // Do nothing when case is sumbission type question
        return null;
      case 2:
        return(
          <Passage />
        )
      default:
        return 'Type 0';
    }
  }

  // Update state for current question UI
  updateQuestionType = (value) => {
    this.setState({
      currentQuestionUI: value
    });
  }

  render() {
    return (
      <div className="container">
        <h3>Question Builder</h3>
        <QuestionsType updateQuestionType={this.updateQuestionType} />
        <QuestionSet>
          <div>{this.renderQuestionUI()}</div>
        </QuestionSet>
      </div>
    )
  }
}

export default Author;

import React, { Component } from 'react';

class Student extends Component {

  render() {
    return (
      <div className="container empty-state">
        <div className="empty-state__inner">
          Oops! Seems like students bunked the class. <span aria-hidden="true">👻 🤡 🤓 🕺</span>
        </div>
      </div>
    )
  }
}

export default Student;

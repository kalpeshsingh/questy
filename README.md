# Question Builder (WIP)

Demo - [Questy on Heroku](https://questy.herokuapp.com/)

## Features

- Supports Multiple Types (MCQs, Passage and Submission)
- Fully Extensible
- Navigation via Keyboard
- Routing
- A Good UI and UX

## My Approach 
Before starting code, I have planned about this on paper and then implemented. 

Builder has 3 main components - 

1. Login: A simple component allows you to choose login as Student or Author.

2. Author: This is the one of the main component where author can compose different types of questions, save them, assignme them etc.

3. Student: This is another component where students can check their questions assigned by authors and answer them.

### Extensibility 
As each author elements are treated as sub-components, you can add as many elements as required.

### Modular
Each component and functions are sort of pure in the nature. The chance of side effect is very less and can be improved further.

### Readability 
Special attention is given to naming convention and commnents in the code.

### Latest and Greatest
Latest ES6 is used in the code. Even the CSS use custom variables to make design easy to modify and extend.